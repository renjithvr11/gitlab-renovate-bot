module.exports = {
  platform: "gitlab",
  endpoint: "https://gitlab.com/api/v4/",
  token: process.env.RENOVATE_TOKEN,
//   repositories: ["renjithvr11/test"],
  requireConfig: true,
  onboarding: true,
  onboardingConfig: {
    extends: ["config:base"],
    prConcurrentLimit: 5,
  },
};
